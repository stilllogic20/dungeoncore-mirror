package net.l_bulb.dungeoncore.item.slot;

public enum SlotType {
  EMPTY, UNAVAILABLE, NORMAL, ADD_EMPTY, REMOVE_UNAVAILABLE, ADDUNKNOW;
}
